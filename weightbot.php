<?php

/**
 * A PHP API for accessing user data from weightbot.com
 *
 **/
class Weightbot {

    private $cookieJar = 'cookiejar';
    private $email;
    private $pass;
    
    var 	$csvData;

    /**
     * Creates a Weightbot object for a given user.
     **/
    public function __construct($email, $pass) {
        $this->email = $email;
        $this->pass = $pass;
    }

    /**
     * Creates a cURL object for remote requests.
     * @param $url string the URL to connect to.
     * @return cURL object
     **/
    private function getCurl($url) {
    
        $ch = curl_init($url);
            
        $curlOptions=array(        
        					CURLOPT_HEADER			=>	false,
        					CURLOPT_RETURNTRANSFER	=>	true,
        					CURLOPT_SSL_VERIFYHOST	=>	false,
        					CURLOPT_SSL_VERIFYPEER	=>	false,
        					CURLOPT_COOKIEJAR		=>	$this->cookieJar,
        					CURLOPT_COOKIEFILE		=>	$this->cookieJar,
        				);

        curl_setopt_array($ch, $curlOptions);        		
        return $ch;
    }
    
    /**
     * Fetches Weightbot page and retrieves the CSRF token in the page's form.
     * @param $url string the remote Weightbot page
     * @return string the CSRF token, if present
     */
    private function getToken($url) {
        $ch = $this->getCurl($url);
        $result = curl_exec($ch);
        curl_close($ch);
        
        $matches = array();
        preg_match("/input name=\"authenticity_token\" type=\"hidden\" value=\"(.*?)\"/", $result, $matches);
        return isset($matches[1]) ? $matches[1] : false;
    }
    
    /**
     * Logs the user in and stores details in the COOKIE_FILE.
     * @return boolean login success/failure
     */
    public function login() {
        $ch = $this->getCurl("https://weightbot.com/account/login");

        $post = array(
            'email' => $this->email,
            'password' => $this->pass,
            'authenticity_token' => $this->getToken("https://weightbot.com/account/login"),
        );
        
        $curlOptions=array(
        					CURLOPT_POST		=>	true,
        					CURLOPT_POSTFIELDS	=>	http_build_query($post),
        				);
        				
        curl_setopt_array($ch, $curlOptions);

        $result = curl_exec($ch);
        curl_close($ch);
        
        // Could be better...
        // @todo yeah, right
        return (strstr($result, 'You are being') !== false);
    }
    
    /**
     * Returns CSV user data
     * @param $cache boolean determines whether pull from cache or from web
     * @return string csv user data
     */
    public function getCsv($cache=1){
    	
    	if($cache)
    	{
    		return $this->getCsvFromCache();
    	}
    	else
    	{
    		return $this->getCsvFromWeb();
    	}
    }
    
    /**
     * Gets the Weightbot CSV user data.
     * @return string csv user data
     */
    public function getCsvFromWeb() {
        $token = $this->getToken("https://weightbot.com");

        // Now that we have the token, we can get the CSV.
        $ch = $this->getCurl("https://weightbot.com/export");
        $post = array(
            'authenticity_token' => $token,
        );
       	
       	$curlOptions=array(
        					CURLOPT_POST		=>	true,
        					CURLOPT_POSTFIELDS	=>	http_build_query($post),
        				);
        				
        curl_setopt_array($ch, $curlOptions);

        $result = curl_exec($ch);
        curl_close($ch);
        
        
        $this->csvData=$result;
        $this->toCache();
        
        return $result;
    }
    
    /**
     * Loads the Weightbot CSV user data from cache
     * @return string csv user data
     */
    public function getCsvFromCache() {
    
    }
    
    /*
    * Stores the CSV data in a local cache
    * @return boolean success/failure
    */
    public function toCache() {
    	
    }
}